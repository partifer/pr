'use strict';

var gulp = require('gulp');
var nodemon = require('gulp-nodemon');

module.exports = gulp.task('serve', function () {
  var options = {
    script: './server/index.js',
    ext: 'js html',
    ignore: ['build/*', 'tmp/*', 'client/*'],
    env: {'NODE_ENV': release ? 'production' : 'development'}
  };
  nodemon(options)
});



