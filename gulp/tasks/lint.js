'use strict';

var gulp = require('gulp');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish'),
    eslint  = require('gulp-eslint');

gulp.task('lint-server', function() {
  return gulp.src(config.paths.src.scriptsServer)
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});

gulp.task('lint-client', function() {
  return gulp.src(config.paths.src.scripts)
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

module.exports = gulp.task('lint', ['lint-client', 'lint-server']);
