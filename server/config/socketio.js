/**
 * Socket.io configuration
 */
'use strict';

import config from "./environment";

function onDisconnect(socket) {
}

function onConnect(socket) {
  socket.on('info', data => {
    socket.log(JSON.stringify(data, null, 2));
  });

  require('../api/food/food.socket').register(socket);
}

export default function (socketio) {
  let options = {
    secret: config.secrets.session,
    handshake: true
  };
  socketio.use(require('socketio-jwt').authorize(options));

  socketio.on('connection', (socket) => {
    socket.address = socket.request.connection.remoteAddress + ':' + socket.request.connection.remotePort;

    socket.connectedAt = new Date();

    socket.log = (...data) => {
      socket.broadcast.to(socket.id).emit('an event', { some: socket.id });
      console.log(`SocketIO ${socket.nsp.name} [${socket.address}]`, ...data);
    };

    // Call onDisconnect.
    socket.on('disconnect', () => {
      onDisconnect(socket);
      socket.log('DISCONNECTED');
    });

    // Call onConnect.
    onConnect(socket);
    socket.log('CONNECTED');
  });
}
