/**
 * Express configuration
 */

'use strict';

import express from "express";
import favicon from "serve-favicon";
import morgan from "morgan";
import compression from "compression";
import bodyParser from "body-parser";
import methodOverride from "method-override";
import cookieParser from "cookie-parser";
import errorHandler from "errorhandler";
import path from "path";
import lusca from "lusca";
import config from "./environment";
import passport from "passport";
import session from "express-session";
import connectMongo from "connect-mongo";
import mongoose from "mongoose";
let mongoStore = connectMongo(session);

export default function (app) {
  let env = app.get('env');

  app.set('views', config.root + '/server/views');
  app.set('view engine', 'jade');
  app.use(compression());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.use(cookieParser());
  app.use(passport.initialize());

  let sessionConfig = {
    secret: config.secrets.session,
    saveUninitialized: true,
    resave: false,
    store: new mongoStore({
      mongooseConnection: mongoose.connection,
      db: 'angular-fullstack'
    })
  };
  app.use(session(sessionConfig));

  /**
   * Lusca - express server security
   * https://github.com/krakenjs/lusca
   */
  if ('test' !== env) {
    app.use(lusca(config.lusca));
  }

  app.set('appPath', path.join(config.root, 'build'));

  if ('production' === env) {
    app.set('appPath', path.join(config.root, 'build'));
  }

  let faviconPath = path.join(app.get('appPath'), 'assets', 'images', 'favicon.ico');
  app.use(favicon(faviconPath));
  app.use(express.static(app.get('appPath')));
  app.use(morgan('dev'));

  if ('development' === env) {
    app.use(require('connect-livereload')());
  }

  if ('development' === env || 'test' === env) {
    app.use(errorHandler()); // Error handler - has to be last
  }
}
