/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import User from "../models/user";
import Food from "../models/food";

User.find({}).remove()
  .then(()=> {
    return Food.find({}).remove();
  })
  .then(() => {
    let testUser = {
      provider: 'local',
      name: 'Test User',
      email: 'test@example.com',
      password: 'test'
    };
    let adminUser = {
      provider: 'local',
      role: 'admin',
      name: 'Admin',
      email: 'admin@admin.com',
      password: 'admin'
    };
    return User.create(testUser, adminUser)
  })
  .then((user) => {
    console.log('finished populating users');
    return user;
  })
  .then((user) => {
    return Food.create({
                  name: 'Куриное филе',
                  creator: user._id,
                  public: true,
                  structure: {
                    protein: 21,
                    fat: 2.5,
                    carbohydrate: 0.6
                  }
                }, {
                  name: 'Филе бедра  курин',
                  creator: user._id,
                  public: true,
                  structure: {
                    protein: 17,
                    fat: 15.7,
                    carbohydrate: 0
                  }
                }, {
                  name: 'Голени куриные',
                  creator: user._id,
                  public: true,
                  structure: {
                    protein: 19,
                    fat: 14.0,
                    carbohydrate: 0
                  }
                }, {
                  name: 'Индейка',
                  creator: user._id,
                  public: true,
                  structure: {
                    protein: 23.5,
                    fat: 1.5,
                    carbohydrate: 0
                  }
                }, {
                  name: 'Говядина',
                  creator: user._id,
                  public: true,
                  structure: {
                    protein: 19,
                    fat: 4.0,
                    carbohydrate: 0
                  }
                });
  })
  .then(() => {
    console.log('finished populating food');
  });
