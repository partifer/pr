'use strict';

// Development specific configuration
// ==================================
module.exports = {

  // MongoDB connection options
  mongo: {
    // debug: true,
    uri: process.env.MONGODB_URI || process.env.MONGOHQ_URL || 'mongodb://localhost/angularfullstack'
  },

  // Seed database on startup
  // seedDB: true

};
