'use strict';

exports = module.exports = {
  // List of user roles
  userRoles: ['guest', 'user', 'admin'],
  lusca: {
    csrf: {
      angular: true
    },
    xframe: 'SAMEORIGIN',
    hsts: {
      maxAge: 31536000, //1 year, in seconds
      includeSubDomains: true,
      preload: true
    },
    xssProtection: true
  }
};
