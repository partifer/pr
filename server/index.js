'use strict';

let config = {
  'plugins': ['add-module-exports'],
  'presets': ['es2015']
};

require('babel-core/register')(config);
exports = module.exports = require('./app');
