'use strict';

import _ from "lodash";
import Food from '../../models/food';

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return (err) => {
    res.status(statusCode).send(err);
  };
}

function responseWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return (entity) => {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
  };
}

function handleEntityNotFound(res) {
  return (entity) => {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function saveUpdates(updates) {
  return (entity) => {
    let updated = _.merge(entity, updates);
    return updated.saveAsync()
      .then(updated => {
        return updated;
      });
  };
}

function removeEntity(res) {
  return (entity) => {
    if (entity) {
      return entity.removeAsync()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function getDefaultSearchParams(req) {
  return {
    deleted: { $ne: true },
    $or: [{ public: true }, { creator: req.user._id }]
  };
}

// Gets a list of Foods
export function index(req, res) {
  Food.find(getDefaultSearchParams(req))
    .sort({ name: 1 })
    .limit(req.query.limit || 10)
    .skip(req.query.offset || 0)
    .execAsync()
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Gets a list of Foods
export function meta(req, res) {
  return Food.countAsync(getDefaultSearchParams(req))
    .then((total) => {
      return { total: total };
    })
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Gets a single Food from the DB
export function show(req, res) {
  Food.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Creates a new Food in the DB
export function create(req, res) {
  req.body.creator = req.user._id;
  Food.createAsync(req.body)
    .then(responseWithResult(res, 201))
    .catch(handleError(res));
}

// Updates an existing Food in the DB
export function update(req, res) {
  if (req.body._id) {
    delete req.body._id;
  }
  Food.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates(req.body))
    .then(responseWithResult(res))
    .catch(handleError(res));
}

// Deletes a Food from the DB
export function destroy(req, res) {
  Food.findByIdAsync(req.params.id)
    .then(handleEntityNotFound(res))
    .then(saveUpdates({ deleted: true }))
    .then(responseWithResult(res))
    .catch(handleError(res));
}
