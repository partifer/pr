'use strict';

import express from 'express';
import controller from './food.controller';
import * as auth from "../../auth/auth.service";

let router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/meta', auth.isAuthenticated(), controller.meta);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.post('/:id', auth.isAuthenticated(), controller.update);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;
