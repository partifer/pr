'use strict';

import { Router } from "express";
import * as controller from "./user.controller";
import * as auth from "../../auth/auth.service";
import upload from "../../components/uploader";

let router = new Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.delete('/:id', auth.hasRole('admin'), controller.destroy);
router.get('/me', auth.isAuthenticated(), controller.me);
router.post('/photo', auth.isAuthenticated(), upload.single('avatar'), controller.photo);
router.put('/:id/password', auth.isAuthenticated(), controller.changePassword);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', controller.create);

export default router;
