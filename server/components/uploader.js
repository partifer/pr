import multer from "multer";
import config from "../config/environment";
import path from 'path';
import fs from 'fs';
import Promise from 'bluebird';

Promise.promisifyAll(fs);

let diskStorageConfig = {
  destination: (req, file, callback) => {
    let dirName = path.join(config.uploader.dest, req.user._id.toString());
    fs.accessAsync(dirName)
      .error(() => {
        return fs.mkdirAsync(dirName);
      })
      .finally(()=> {
        return callback(null, dirName);
      });
  },
  filename: (req, file, callback) => {
    callback(null, file.fieldname + '-' + Date.now());
  }
};

let storage = multer.diskStorage(diskStorageConfig);

let multerConfig = {
  storage: storage,
  limits: {
    fileSize: config.uploader.size,
    files: 1,
    fields: 1
  },
  fileFilter: (req, file, cb) => {
    let isImage = config.uploader.mimetype.indexOf(file.mimetype) !== -1;
    cb(isImage ? null : new Error('Wrong mimetype'), isImage);
  }
};
let upload = multer(multerConfig);

export default upload;
