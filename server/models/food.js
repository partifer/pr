'use strict';

import mongoose from 'mongoose';

let FoodSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: { type: String },
  structure: {
    protein: { type: Number, min: 0, max: 100, required: true },
    fat: { type: Number, min: 0, max: 100, required: true },
    carbohydrate: { type: Number, min: 0, max: 100, required: true },
    calories: {
      type: Number,
      min: 0
    }
  },
  deleted: {
    type: Boolean
  },
  public: {
    type: Boolean,
    required: true,
    default: false
  },
  creator: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  }
});

FoodSchema
  .pre('save', next => {
    if(!this.structure.calories) {
      this.structure.calories = Math.round(this.structure.protein * 4 + this.structure.fat * 9 + this.structure.carbohydrate * 4);
    }
    next();
  });

export default mongoose.model('Food', FoodSchema);
