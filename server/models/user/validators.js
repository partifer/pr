const authTypes = ['github', 'twitter', 'facebook', 'google'];

export function emptyEmail(email) {
  if (authTypes.indexOf(this.provider) !== -1) {
    return true;
  }
  return email.length;
}

export function emptyPassword (password) {
  if (authTypes.indexOf(this.provider) !== -1) {
    return true;
  }
  return password.length;
}

export function takenEmail (value, respond) {
  let self = this;
  return this.constructor.findOne({ email: value })
    .then((user) => {
      if (user) {
        if (self.id === user.id) {
          return respond(true) || null;
        }
        return respond(false) || null;
      }
      return respond(true) || null;
    })
    .catch((err) => {
      throw err;
    })
}
