import crypto from "crypto";

/**
 * Authenticate - check if the passwords are the same
 *
 * @param {String} password
 * @param {Function} callback
 * @return {Boolean}
 * @api public
 */
export function authenticate(password, callback) {
  if (!callback) {
    return this.password === this.encryptPassword(password);
  }

  this.encryptPassword(password, (err, pwdGen) => {
    if (err) {
      return callback(err);
    }

    if (this.password === pwdGen) {
      callback(null, true);
    } else {
      callback(null, false);
    }
  });
}

/**
 * Make salt
 *
 * @param {Number} byteSize Optional salt byte size, default to 16
 * @param {Function} callback
 * @return {String}
 * @api public
 */
export function makeSalt(byteSize, callback) {
  let defaultByteSize = 16;

  if (typeof arguments[0] === 'function') {
    callback = arguments[0];
    byteSize = defaultByteSize;
  } else if (typeof arguments[1] === 'function') {
    callback = arguments[1];
  }

  if (!byteSize) {
    byteSize = defaultByteSize;
  }

  if (!callback) {
    return crypto.randomBytes(byteSize).toString('base64');
  }

  return crypto.randomBytes(byteSize, (err, salt) => {
    if (err) {
      callback(err);
    } else {
      callback(null, salt.toString('base64'));
    }
  });
}

/**
 * Encrypt password
 *
 * @param {String} password
 * @param {Function} callback
 * @return {String}
 * @api public
 */
export function encryptPassword(password, callback) {
  if (!password || !this.salt) {
    return null;
  }

  let defaultIterations = 10000;
  let defaultKeyLength = 64;
  let salt = new Buffer(this.salt, 'base64');

  if (!callback) {
    return crypto.pbkdf2Sync(password, salt, defaultIterations, defaultKeyLength)
      .toString('base64');
  }

  return crypto.pbkdf2(password, salt, defaultIterations, defaultKeyLength, (err, key) => {
    if (err) {
      callback(err);
    } else {
      callback(null, key.toString('base64'));
    }
  });
}
