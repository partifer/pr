'use strict';

import { Schema } from "mongoose";
import mongoose from 'mongoose';
import * as validators from './validators';
import * as methods from './methods';

let UserSchema = new Schema({
  name: String,
  email: {
    type: String,
    lowercase: true
  },
  role: {
    type: String,
    default: 'user'
  },
  score: {
    used: {
      type: Number,
      default: 0
    },
    bought: {
      type: Number,
      default: 0
    },
    experience: {
      type: Number,
      default: 0
    },
    total: {
      type: Number,
      default: 0
    }
  },
  password: String,
  provider: String,
  salt: String,
  facebook: {},
  twitter: {},
  google: {},
  github: {},
  timestamps: {
    login: { type: Date, default: Date.now },
    active: { type: Date, default: Date.now },
    created: { type: Date, default: Date.now },
    updated: { type: Date, default: Date.now }
  }
});

/**
 * Virtuals
 */

// Public profile information
UserSchema
  .virtual('profile')
  .get(() => {
    return {
      'name': this.name,
      'role': this.role
    };
  });

// Non-sensitive info we'll be putting in the token
UserSchema
  .virtual('token')
  .get(() => {
    return {
      '_id': this._id,
      'role': this.role
    };
  });

/**
 * Validations
 */
UserSchema.path('email').validate(validators.emptyEmail, 'Email cannot be blank');
UserSchema.path('password').validate(validators.emptyPassword, 'Password cannot be blank');
UserSchema.path('email').validate(validators.takenEmail, 'The specified email address is already in use.');

/**
 * Pre-save hook
 */
UserSchema
  .pre('save', (next) => {
    // Handle new/update passwords
    if (!this.isModified('password')) {
      return next();
    }

    let validatePresenceOf = (value) => {
      return value && value.length;
    };


    if (!validatePresenceOf(this.password) && authTypes.indexOf(this.provider) === -1) {
      next(new Error('Invalid password'));
    }

    // Make salt with a callback
    this.makeSalt((saltErr, salt) => {
      if (saltErr) {
        next(saltErr);
      }
      this.salt = salt;
      this.encryptPassword(this.password, (encryptErr, hashedPassword) => {
        if (encryptErr) {
          next(encryptErr);
        }
        this.password = hashedPassword;
        next();
      });
    });
  });

/**
 * Pre-save hook
 */
UserSchema
  .pre('save', (next) => {
    this.score.total = this.score.experience + this.score.bought - this.score.used;
    return next();
  });

UserSchema
  .pre('save', (next) => {
    let now = new Date();
    this.timestamps.created = this.timestamps.created || now;
    this.timestamps.updated = this.timestamps.updated || now;
    this.timestamps.login = this.timestamps.login || now;
    this.timestamps.active = this.timestamps.active || now;
    next();
  });

/**
 * Methods
 */
UserSchema.methods = methods;

export default mongoose.model('User', UserSchema);
