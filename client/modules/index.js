require('angular');
require('angular-cookies');
require('angular-ui-bootstrap');
require('angular-resource');
require('angular-ui-router');
require('angular-file-upload');
require('angular-socket-io');
require('./app');
