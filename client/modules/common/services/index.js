module.exports =
  angular.module('client.common.services', ['btford.socket-io'])
    .factory('socket', require('./socket'));
