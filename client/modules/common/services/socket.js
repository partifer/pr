/* global io */

/* @ngInject */
function SocketUpdaterFactory(_, Auth, socketFactory) {
  var ioSocket = io('', {
    query: 'token=' + Auth.getToken(),
    path: '/socket.io-client'
  });

  var socket = socketFactory({ ioSocket });

  return {
    socket,
    syncUpdates(modelName, array, cb) {
      cb = cb || angular.noop;

      socket.on(modelName + ':save', function (item) {
        var oldItem = _.find(array, { _id: item._id });
        var event = 'created';

        if (oldItem) {
          angular.extend(oldItem, item);
          event = 'updated';
        } else {
          array.push(item);
        }

        cb(event, item, array);
      });

      socket.on(modelName + ':remove', function (item) {
        var event = 'deleted';
        _.remove(array, { _id: item._id });
        cb(event, item, array);
      });
    },

    unsyncUpdates(modelName) {
      socket.removeAllListeners(modelName + ':save');
      socket.removeAllListeners(modelName + ':remove');
    }
  };
}

module.exports = SocketUpdaterFactory;
