module.exports =
  angular.module('client.common', [
    require('./directives').name,
    require('./filters').name,
    require('./services').name,

    require('./oauth-buttons').name,
    require('./navbar').name
  ]);
