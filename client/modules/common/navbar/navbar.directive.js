module.exports = function () {
  return {
    templateUrl: 'common/navbar/navbar',
    restrict: 'E',
    controller: 'NavbarController',
    controllerAs: 'nav'
  };
};
