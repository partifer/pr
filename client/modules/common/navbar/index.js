module.exports =
  angular.module('client.navbar', [])
    .controller('NavbarController', require('./navbar.controller'))
    .directive('navbar', require('./navbar.directive'))
;
