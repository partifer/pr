class NavbarController {
  /* @ngInject */
  constructor(Auth) {
    this.menu = [{
      title: 'Home',
      state: 'main'
    }];

    this.isCollapsed = true;

    this.isLoggedIn = Auth.isLoggedIn;
    this.isAdmin = Auth.isAdmin;
    this.getCurrentUser = Auth.getCurrentUser;
  }
}

module.exports = NavbarController;
