module.exports =
  angular.module('client.common.directives', [])
    .directive('mongooseError', function () {
      return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
          element.on('keydown', () => ngModel.$setValidity('mongoose', true));
        }
      };
    });
