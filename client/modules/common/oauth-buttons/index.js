module.exports =
  angular.module('client.authBtn', [])
    .controller('OauthButtonsCtrl', require('./oauth-buttons.controller'))
    .directive('oauthButtons', require('./oauth-buttons.directive'))
;
