/** @ngInject */
module.exports = function () {
  return {
    templateUrl: 'common/oauth-buttons/oauth-buttons',
    restrict: 'EA',
    controller: 'OauthButtonsCtrl',
    controllerAs: 'OauthButtons',
    scope: {
      classes: '@'
    }
  };
};
