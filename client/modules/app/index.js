module.exports =
  angular.module('client', [
    'ui.bootstrap',
    'ui.router',
    'ngCookies',
    'ngResource',
    'angularFileUpload',
    require('../../../tmp/templates').name,
    require('../common').name,
    require('./util').name,
    require('./main').name,
    require('./auth').name,
    require('./profile').name,
    require('./food').name
  ]);

angular.module('client.constants', [])
  .constant('appConfig', { userRoles: ['guest', 'user', 'admin'] })
  .constant('_', require('lodash'))
;
