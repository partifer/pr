/** @ngInject */
module.exports = function (_, $rootScope, $state, Auth) {
  $rootScope.$on('$stateChangeSuccess', function (event, current) {
    $rootScope.title = current.title;
  });

  $rootScope.$on('$stateChangeStart', function (event, next) {
    if (!next.authenticate) {
      return;
    }

    if (typeof next.authenticate === 'string') {
      Auth.hasRole(next.authenticate, _.noop).then(has => {
        if (has) {
          return _.noop;
        }

        event.preventDefault();
        return Auth.isLoggedIn(_.noop).then(is => {
          $state.go(is ? 'main' : 'app.anon.login');
        });
      });
    } else {
      Auth.isLoggedIn(_.noop).then(is => {
        if (is) {
          return;
        }

        event.preventDefault();
        $state.go('app.anon.login');
      });
    }
  });
};
