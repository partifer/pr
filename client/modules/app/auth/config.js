/** @ngInject */
module.exports = function ($httpProvider, $stateProvider, $locationProvider) {
  $httpProvider.interceptors.push('authInterceptor');

  $stateProvider
    .state('app.anon.login', {
      url: '/login',
      title: 'Login',
      templateUrl: 'app/auth/views/login',
      controller: 'loginCtrl',
      controllerAs: 'vm'
    })
    .state('app.anon.signup', {
      url: '/signup',
      templateUrl: 'app/auth/views/signup',
      controller: 'signUpCtrl',
      controllerAs: 'vm'
    })
    .state('app.anon.logout', {
      url: '/logout?referrer',
      referrer: 'app.anon.login',
      template: '',
      controller: 'logoutCtrl'
    });

  $locationProvider.html5Mode(true);
};
