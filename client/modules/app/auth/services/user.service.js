/* @ngInject */
function UserResource($resource) {
  return $resource(
    '/api/users/:id/:controller', {
      id: '@_id'
    }, {
      changePassword: {
        method: 'PUT',
        params: {
          controller: 'password'
        }
      },
      get: {
        method: 'GET',
        params: {
          id: 'me'
        }
      },
      send: {
        method: 'PUT',
        params: {
          controller: 'message',
          message: 'asdf'
        }
      }
    }
  );
}

module.exports = UserResource;
