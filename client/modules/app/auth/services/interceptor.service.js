/** @ngInject */
module.exports = function authInterceptor($rootScope, $q, $cookies, $injector, Util) {
  var state;
  return {
    // Add authorization token to headers
    request(config) {
      config.headers = config.headers || {};
      if ($cookies.get('token') && Util.isSameOrigin(config.url)) {
        config.headers.Authorization = 'Bearer ' + $cookies.get('token');
      }
      return config;
    },

    // Intercept 401s and redirect you to login
    responseError(response) {
      if (response.status === 401) {
        $cookies.remove('token');
        (state || (state = $injector.get('$state'))).go('app.anon.login');
        // remove any stale tokens
      }
      return $q.reject(response);
    }
  };
};
