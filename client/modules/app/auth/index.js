module.exports =
  angular.module('client.auth', ['client.constants',
                                 'client.util',
                                 'ngCookies',
                                 'ui.router'])
    .config(require('./config'))
    .factory('Auth', require('./services/auth.service'))
    .factory('authInterceptor', require('./services/interceptor.service'))
    .factory('User', require('./services/user.service'))

    .controller('loginCtrl', require('./controllers/login.controller'))
    .controller('logoutCtrl', require('./controllers/logout.controller'))
    .controller('signUpCtrl', require('./controllers/signup.controller'))

    .run(require('./run'))
;
