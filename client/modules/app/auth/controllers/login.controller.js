class LoginController {
  /* @ngInject */
  constructor(Auth, $state) {
    this.user = {};
    this.errors = {};
    this.submitted = false;

    this.Auth = Auth;
    this.$state = $state;
  }

  login(form) {
    this.submitted = true;

    if (form.$valid) {
      let params = {
        email: this.user.email,
        password: this.user.password
      };
      this.Auth.login(params)
        .then(() => {
          this.$state.go('main');
        })
        .catch(err => {
          this.errors.other = err.message;
        });
    }
  }
}

module.exports = LoginController;
