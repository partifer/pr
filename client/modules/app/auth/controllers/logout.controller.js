class LogoutController {
  /* @ngInject */
  constructor($state, Auth) {
    var referrer = $state.params.referrer || $state.current.referrer || 'app.anon.login';
    Auth.logout();
    $state.go(referrer);
  }
}

module.exports = LogoutController;
