class ProfileController {
  /* @ngInject */
  constructor($cookies, Auth, FileUploader) {
    this.getCurrentUser = Auth.getCurrentUser;
    this.uploader = new FileUploader({
      url: '/api/users/photo',
      alias: 'avatar',
      headers: {
        Authorization: 'Bearer ' + $cookies.get('token'),
        'X-XSRF-TOKEN': $cookies.get('XSRF-TOKEN')
      }
    });
  }
}

module.exports = ProfileController;
