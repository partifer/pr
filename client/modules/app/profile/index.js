module.exports =
  angular.module('client.profile', ['client.constants',
                                    'client.util',
                                    'ngCookies',
                                    'ui.router'])
    .config(require('./config'))
    .controller('profileCtrl', require('./controllers/profile.controller'))
;
