/** @ngInject */
module.exports = function ($httpProvider, $stateProvider) {
  $httpProvider.interceptors.push('authInterceptor');

  $stateProvider
    .state('app.user.profile', {
      url: '/profile',
      title: 'Profile',
      templateUrl: 'app/profile/views/profile',
      controller: 'profileCtrl',
      controllerAs: 'profile'
    });
};
