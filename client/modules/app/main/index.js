module.exports =
  angular.module('client.foo', [])
    .config(function ($stateProvider) {
      $stateProvider
        .state('app', {
          url: '',
          template: '<ui-view/>',
          abstract: true
        })
        .state('app.anon', {
          url: '',
          template: '<ui-view/>',
          abstract: true
        })
        .state('app.user', {
          url: '',
          template: '<ui-view/>',
          abstract: true,
          authenticate: true
        })
        .state('app.admin', {
          url: '',
          template: '<ui-view/>',
          abstract: true,
          authenticate: 'admin'
        })
        .state('main', {
          url: '',
          templateUrl: 'app/main/layout',
          controller: 'defaultController'
        })
      ;
    })
    .controller('defaultController', require('./default.controller'));
