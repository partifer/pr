/* @ngInject */
function UtilService($window) {
  let Util = {

    safeCb(cb) {
      return (angular.isFunction(cb)) ? cb : angular.noop;
    },

    urlParse(url) {
      var a = document.createElement('a');
      a.href = url;
      return a;
    },

    isSameOrigin(url, origins) {
      url = this.urlParse(url);
      origins = (origins && [].concat(origins)) || [];
      origins = origins.map(this.urlParse);
      origins.push($window.location);
      origins = origins.filter(function (o) {
        return url.hostname === o.hostname &&
               url.port === o.port &&
               url.protocol === o.protocol;
      });
      return (origins.length >= 1);
    }
  };

  return Util;
}

module.exports = UtilService;
