class ProfileController {
  /* @ngInject */
  constructor($scope, Food, socket) {
    this.Food = Food;
    this.foods = Food.query();
    this.meta = Food.meta();

    socket.syncUpdates('food', this.foods);

    $scope.$on('$destroy', () => {
      socket.unsyncUpdates('food');
    });
  }

  add() {
    this.foods.push(new this.Food({ structure: {} }));
    this.meta.total += 1;
  }

  pageChanged() {
    var params = {
      limit: this.meta.limit,
      offset: (this.meta.current - 1) * this.meta.limit
    };
    this.foods = this.Food.query(params);
  }

  deleteFunction() {
    this.meta.total -= 1;
    this.pageChanged();
  }

  checkDeleted(obj) {
    return !obj.deleted;
  }
}

module.exports = ProfileController;
