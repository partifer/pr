module.exports =
  angular.module('client.food', ['client.constants',
                                 'client.common.services',
                                 'client.util',
                                 'ngCookies',
                                 'ui.router'])
    .config(require('./config'))
    .factory('Food', require('./services/food.service'))

    .directive('food', require('./directive/food'))

    .controller('foodCtrl', require('./controllers/food.controller'))
;
