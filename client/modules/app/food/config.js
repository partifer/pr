/** @ngInject */
module.exports = function ($httpProvider, $stateProvider) {
  $stateProvider
    .state('app.user.food', {
      url: '/food',
      title: 'Food',
      templateUrl: 'app/food/views/index',
      controller: 'foodCtrl',
      controllerAs: 'vm'
    });
};
