/* @ngInject */
function UserResource($resource) {
  let defaultLimit = 10;
  return $resource(
    '/api/food/:id',
    {
      id: '@_id',
      limit: '@limit'
    }, {
      query: {
        method: 'GET',
        isArray: true,
        params: {
          limit: defaultLimit
        }
      },
      meta: {
        method: 'GET',
        params: {
          id: 'meta',
          limit: defaultLimit
        },
        transformResponse: function (data) {
          var parse = JSON.parse(data);
          parse.limit = defaultLimit;
          return parse;
        }
      }
    }
  );
}

module.exports = UserResource;
