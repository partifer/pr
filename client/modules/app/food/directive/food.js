var getProperty = function (obj, prop) {
  var props = prop.split('.');
  if (props.length === 1) return obj[prop];
  return getProperty(obj[props.shift()], props.join('.'));
};

class FoodController {
  /* @ngInject */
  constructor($scope) {
    this.$scope = $scope;
    this.food = $scope.food;
    this.errors = {};
    this.loading = false;
  }

  save(form) {
    this.loading = true;
    this.submitted = true;

    if (form.$valid) {
      this.food.$save()
        .then(() => {
          this.loading = false;
        })
        .catch(err => {
          this.loading = false;
          err = err.data;
          this.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, (error, field) => {
            // console.log(getProperty(form, field));
            form[field].$setValidity('mongoose', false);
            this.errors[field] = error.message;
          });
        });
    } else {
      this.loading = false;
    }
  }

  delete() {
    var delFunction = this.$scope.delete;
    this.food.$delete()
      .then(function () {
        delFunction({});
      });
  }

  countCalories() {
    var structure = this.food.structure || {};
    var protein = structure.protein || 0;
    var fat = structure.fat || 0;
    var carbohydrate = structure.carbohydrate || 0;
    return Math.round(protein * 4 + fat * 9 + carbohydrate * 4);
  }
}

module.exports = function () {
  return {
    templateUrl: 'app/food/views/food',
    restrict: 'AE',
    controllerAs: 'fdCtrl',
    scope: {
      food: '=',
      pageChanged: '&',
      delete: '&'
    },
    controller: FoodController
  };
};
